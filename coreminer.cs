private const string PreFix = "CoMi";
private const float PistonSpeed = .004f;
private const float RotorSpeed = 2.1f;

private string state;
private StringBuilder log = new StringBuilder();
private StateMachine<string> stateMachine;

private List<IMyPistonBase> pistons = new List<IMyPistonBase>();
private List<IMyShipWelder> welders = new List<IMyShipWelder>();
private List<IMyLandingGear> landingGear = new List<IMyLandingGear>();
private List<IMyProjector> projectors = new List<IMyProjector>();
private List<IMyMotorAdvancedStator> rotors = new List<IMyMotorAdvancedStator>();
private List<IMyShipDrill> drills = new List<IMyShipDrill>();
private List<IMyShipMergeBlock> merger = new List<IMyShipMergeBlock>();
private List<IMyShipConnector> connectors = new List<IMyShipConnector>();
private List<IMyCargoContainer> storage = new List<IMyCargoContainer>();


public Program()
{
    if(string.IsNullOrWhiteSpace(Storage))
    {
        state = "Stop";
    }
    else
    {
        state = Storage;
    }

    if(InitCheck(log) == "Init")
    {
        state = "Init";
    }

    CreateStateMachine();

    Runtime.UpdateFrequency = UpdateFrequency.Update10;
}

public void Save()
{
    Storage = state;
}

private void CreateStateMachine()
{
    this.stateMachine = new StateMachine<string>()
    {
        CurrentState = state
    };

    stateMachine.AddState(
        new MachineState<string>(
            "Stop", (o) => "Stop", StopStuff));

    stateMachine.AddState(
        new MachineState<string>(
            "Init", InitCheck));

    stateMachine.AddState(
        new MachineState<string>(
            "Mine", MineCheck, MinePrep));

    stateMachine.AddState(
        new MachineState<string>(
            "LockGears", LockGearsCheck, LockGearsPrep));

    stateMachine.AddState(
        new MachineState<string>(
            "PullDown", PullDownCheck, PullDownPrep));

    stateMachine.AddState(
        new MachineState<string>(
            "Remerge", RemergeCheck, RemergePrep));
    stateMachine.AddState(
        new MachineState<string>(
            "Pause", PauseCheck, o => { SetPistonSpeed(0); return true; }));
}

private string PauseCheck(StringBuilder logger)
{
    var cfr = GetCargoFillRatio();
    logger.AppendLine($"Cargo @ {cfr:P}");

    if (cfr < .2)
    {
        return "Mine";
    }

    return "Pause";
}

private bool StopStuff(StringBuilder arg)
{
    SetPistonSpeed(0);
    SetAll(drills, false);
    SetAll(projectors, false);
    SetRotorSpeed(0);
    LockLandingGear(true);

    return true;
}

private string RemergeCheck(StringBuilder logger)
{
    var tpe = TotalPistonExtension(logger);

    if(AllMergeConnected() && tpe <= 0)
    {
        return "Mine";
    }

    return "Remerge";
}

private bool RemergePrep(StringBuilder logger)
{
    SetAll(merger, true);
    return true;
}

private string PullDownCheck(StringBuilder logger)
{
    if(TotalPistonExtension(logger)<.5)
    {
        return "Remerge";
    }

    return "PullDown";
}

private bool PullDownPrep(StringBuilder logger)
{
    SetAll(merger, false);
    LockConnectors(false);
    SetPistonSpeed(-.5f);
    return true;
}

private string LockGearsCheck(StringBuilder logger)
{
    if(AllGearsLocked())
    {
        return "PullDown";
    }

    return "LockGears";
}

private bool AllGearsLocked()
{
    foreach(var gear in landingGear)
    {
        if(!gear.IsLocked)
        {
            return false;
        }
    }

    return true;
}

private bool LockGearsPrep(StringBuilder arg)
{
    LockLandingGear(true);
    SetAll(projectors, false);
    SetAll(welders, false);
    SetRotorSpeed(0);
    SetAll(drills, false);
    SetPistonSpeed(0);

    return true;
}

private string MineCheck(StringBuilder logger)
{
    var ps = TotalPistonExtension(logger);
    var cfr = GetCargoFillRatio();
    logger.AppendLine($"pistons @ {ps:P}");
    logger.AppendLine($"pistons set {PistonSpeed:F3}");
    logger.AppendLine($"Cargo @ {cfr:P}");

    if(cfr >.9)
    {
        return "Pause";
    }

    if (ps <1)
    {
        return "Mine";
    }

    return "LockGears";
}

private double TotalPistonExtension(StringBuilder logger)
{
    var count = 0d;
    var pct = 0.0;

    foreach (var piston in pistons)
    {
        ++count;
        pct += MathHelper.PercentInRange(piston.CurrentPosition, piston.MinLimit, piston.MaxLimit);
    }

    return pct / count;
}

private bool MinePrep(StringBuilder logger)
{
    if(! AllMergeConnected())
    {
        return false;
    }
    var ps = TotalPistonExtension(logger);

    LockLandingGear(false);
    LockConnectors(true);
    SetAll(projectors, true);
    SetAll(welders, true);
    SetRotorSpeed(RotorSpeed);
    SetAll(drills, true);
    SetPistonSpeed(PistonSpeed);
    logger.AppendLine($"pistons @ {ps:P}");
    logger.AppendLine($"pistons set {PistonSpeed:F4}");

    //TODO check projectors for on ?

    return true;
}

private void LockConnectors(bool setMe)
{
    foreach(var conn in connectors)
    {
        if(setMe)
        {
            conn.Connect();
        }
        else
        {
            conn.Disconnect();
        }
    }
}

private void SetRotorSpeed(float umin)
{
    foreach(var rotor in rotors)
    {
        rotor.TargetVelocityRPM = umin;
    }
}

private void LockLandingGear(bool setTo)
{
    foreach(var gear in landingGear)
    {
        if (setTo)
        {
            gear.Lock();
        }
        else
        {
            gear.Unlock();
        }

    }
}

private void SetPistonSpeed(float velo)
{
    foreach(var piston in pistons)
    {
        piston.Velocity = velo;
    }
}

private bool AllMergeConnected()
{
    if(merger.Count <1)
    {
        return false;
    }

    foreach(var merge in merger)
    {
        if(!merge.IsConnected)
        {
            return false;
        }
    }

    return true;
}

private void SetAll<T>(List<T> blocks, bool setTo) where T: IMyFunctionalBlock
{
    foreach(var block in blocks)
    {
        block.Enabled = setTo;
    }
}

private string InitCheck(StringBuilder logger)
{

    if(!GetStuff(pistons, "piston(s)", logger))
    {
        return "Init";
    }

    if (!GetStuff(welders, "welder(s)", logger))
    {
        return "Init";
    }

    if (!GetStuff(landingGear, "LGear(s)", logger))
    {
        return "Init";
    }

    if (!GetStuff(projectors, "Projectors(s)", logger))
    {
        return "Init";
    }

    if (!GetStuff(rotors, "Rotor(s)", logger))
    {
        return "Init";
    }

    if (!GetStuff(drills, "Drill(s)", logger))
    {
        return "Init";
    }

    if (!GetStuff(merger, "Merge Block(s)", logger))
    {
        return "Init";
    }

    if (!GetStuff(connectors, "Connector(s)", logger))
    {
        return "Init";
    }


    if (!GetStuff(storage, "Cargo Container", logger))
    {
        return "Init";
    }

    //TODO Check if all merges connected

    if (AllMergeConnected())
    {
        return "Mine";
    }

    logger.AppendLine("not all merge blocks connected");
    return "Init";
}

/// <summary>
        /// 0..1
        /// </summary>
        /// <returns></returns>
double GetCargoFillRatio()
{
    var current = 0d;
    var max = 0d;

    foreach (var cont in storage)
    {
        max += (double)cont.GetInventory().MaxVolume;
        current += (double)cont.GetInventory().CurrentVolume;
    }

    return current / max;
}



private bool GetStuff<T>(List<T> list, string name, StringBuilder logger) where T: class, IMyTerminalBlock
{
    GridTerminalSystem.GetBlocksOfType(list, b => b.IsSameConstructAs(Me) && b.CustomName.StartsWith(PreFix));

    if (list.Count <= 0)
    {
        logger.AppendLine($"no {name} with name {PreFix}.. found");
        return false;
    }

    return true;
}

public void Main(string argument, UpdateType updateSource)
{
    if(argument.Equals("Stop"))
    {
        stateMachine.CurrentState = "Stop";
        return;
    }

    if (argument.Equals("Init"))
    {
        stateMachine.CurrentState = "Init";
        return;
    }

    log.Clear();
    log.AppendLine("-- Core Miner V0.2 --");
    log.AppendLine(DateTime.Now.ToString("HH:mm:ss"));
    log.AppendLine($"State: {state ?? "unknown"}");
    log.AppendLine("Info:");

    stateMachine.Go(log);

    state = stateMachine.CurrentState;
    Save();
    Echo(log.ToString());
    IGC.SendBroadcastMessage("CoMi", log.ToString());
    Display.SetText(Me.GetSurface(0), log.ToString(), bg: Color.Black, fg: Color.Orange,fontsize: .8f);
}

}

public class Display
{
    public static string getBar(double percent, int width, bool addbrackets = false)
    {
        if (percent > 100)
            return "out of range";

        if (percent < 0)
            return "out of range";

        int numSymbols = addbrackets ? width - 2 : width;

        var onsymbolcount = (int)((numSymbols * percent) / 100d);

        var sb = new StringBuilder();

        if (addbrackets)
            sb.Append("[");

        if (onsymbolcount > 0)
            sb.Append(new string('█', onsymbolcount));

        if (numSymbols - onsymbolcount > 0)
            sb.Append(new string('░', numSymbols - onsymbolcount));

        if (addbrackets)
            sb.Append("]");


        return sb.ToString();
    }


    public static void SetText(IMyTextSurface surf, string txt,Color? bg = null, Color? fg = null, TextAlignment? align = null, float fontsize = 1f)
    {
        surf.ContentType = ContentType.TEXT_AND_IMAGE;
        surf.BackgroundColor = bg ?? Color.Black;
        surf.FontColor = fg ?? Color.White;
        surf.FontSize = fontsize;
        surf.Font = "Monospace";
        surf.Alignment = align ?? TextAlignment.LEFT;
        surf.WriteText(txt ?? string.Empty);
    }

    public static int GetSurfaceWidth(IMyTextSurface srf, float fontsize)
    {
        var w10 = srf.MeasureStringInPixels(new StringBuilder("0123456789"), "Monospace", fontsize);

        var availablespaceX10 = srf.SurfaceSize.X * (100 - srf.TextPadding) / 10; // 10 * size*  (100% - padding%)/100  =  size(percent)/10

        return (int)Math.Floor(availablespaceX10 / w10.X);
    }
}

public partial class MathHelper
{
    /// <summary>
        /// 0..1
        /// </summary>
        /// <param name="current"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
    public static double PercentInRange(double current, double min, double max)
    {
        var total = max - min;

        var currInRange = current - min;

        return currInRange / total;

    }


    static string[] sizes = { "", "K", "M", "G", "T" };
    public static  Result2<double, string> DoubleToHumanReadable(double d)
    {
        var order = (int)Math.Floor(Math.Log10(d)/3);
        if(order >= sizes.Length)
        {
            order = sizes.Length - 1;
        }

        if (order < 0)
        {
            order = 0;
        }

        return new Result2<double, string>(d / (Math.Pow(10, order*3)), sizes[order]);
    }

}


public class Result2<T,K>
{
    public readonly T One;
    public readonly K Two;

    public Result2(T one , K two)
    {
        One = one;
        Two = two;
    }
}

public class StateMachine<T>
{
    private Dictionary<T, MachineState<T>> states = new Dictionary<T, MachineState<T>>();
    private T currentState;
    public T CurrentState
    {
        get
        {
            return currentState;
        }

        set
        {
            if(value == null)
            {
                return;
            }

            if(value.Equals(currentState))
            {
                return;
            }

            currentState = value;
            ResetPrepared();
        }
    }

    private void ResetPrepared()
    {
        foreach(var ms in states.Values)
        {
            ms.ResetPrepared();
        }
    }

    public void AddState(MachineState<T> state)
    {
        states[state.State] = state;
    }

    public void Go(StringBuilder sb)
    {
        if(CurrentState == null)
        {
            sb?.AppendLine("no state set");
            return;
        }

        if(!states.ContainsKey(CurrentState))
        {
            sb?.AppendLine("no action for current state");
            return;
        }

        CurrentState = states[CurrentState].Go(sb);
    }

}


public class MachineState<T>
{
    public readonly T State;
    public readonly Func<StringBuilder, bool> Prepare;
    public readonly Action<StringBuilder> Run;
    public readonly Func<StringBuilder,T> Check;

    private bool prepared;

    public MachineState(T state, Func<StringBuilder, T>check, Func<StringBuilder, bool> prep = null, Action<StringBuilder> run = null)
    {
        this.State = state;
        this.Prepare = prep;
        this.Run = run;
        this.Check = check;
    }

    public T Go(StringBuilder sb)
    {
        if(!prepared)
        {
            if(Prepare != null)
            {
                prepared = Prepare(sb);
                return State;
            }
        }

        Run?.Invoke(sb);

        var result = Check(sb);

        if (!result.Equals(State))
        {
            prepared = false;
        }

        return result;
    }

    internal void ResetPrepared()
    {
        prepared = false;
    }